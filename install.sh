#!/bin/bash

generatescan () {
	cat > /usr/bin/usbcp-scan << EOM
	#!/usr/bin/env python
	import pyudev, subprocess, sys	#uvozimo knjižnico pyudev (libudev za python)

	def main(argv):
		context = pyudev.Context()	#povežemo pyudev z udev bazo naprav
		print(sys.argv[1])
		monitor = pyudev.Monitor.from_netlink(context)	
		monitor.filter_by('block')
		for device in iter(monitor.poll, None):
			if 'ID_FS_TYPE' in device:
				#print('{0} partition {1}, type {2}'.format(device.action, device.device_node, device.device_type))
				if device.device_type == "partition" and device.action == "add":
					print("Zaznana veljavna particija {0}, tipa {1}.".format(device.device_node, device.get('ID_FS_TYPE')))
					output = subprocess.check_output(['usbcp-copy', device.device_node, sys.argv[1]])
					print(output[:-1].decode())

	if __name__ == '__main__':
	    main(sys.argv[1:])
	EOM
}

generatecp () {
	cat > /usr/bin/usbcp-scan << EOM
	#!/bin/bash
	ismounted=$(cat /proc/mounts | grep -c $1)	#preverimo, če je na novo vstavljen pogon mountan

	if [ $ismounted -lt 1 ]; then	#če ni, moramo to narediti sami
		udisksctl mount -b $1 &> /dev/null
	fi

	pot=$(lsblk -p | grep $1 | awk '{ print $7 }')	#najdemo direktorij, v katerega je pogon vpet

	cp $2 $pot/		#kopiramo datoteko na pogon	

	md5orig=`md5sum $2 | awk '{ print $1 }'`	#generiramo checksum originalne datoteke
	md5copy=`md5sum $pot/$2 | awk '{ print $1 }'`	#generiramo checksum kopirane datoteke

	if [ $md5orig != $md5copy ]; then	#preverimo, če se vrednosti ujemata.
		echo "Napaka v prenosu datoteke."
		#notify-send "Napaka v kopiranju datoteke \"$2\" na pogon $1."
	else
		echo "Datoteka uspesno prenesena."
		#notify-send "Datoteka \"$2\" uspesno kopirana na pogon $1 ($pot/$2)."
	fi
	EOM
}

install () {
	curl https://bootstrap.pypa.io/get-pip.py > get-pip.py
	sudo python get-pip.py
	rm get-pip.py
	pip install pyudev
	cp usb* /usr/bin
	generatescan
	generatecp
	chmod +x /usr/bin/usbcp-*
}

[ "$UID" -eq 0 ] || exec sudo bash "$0" "$@"	#potrebujemo root
install